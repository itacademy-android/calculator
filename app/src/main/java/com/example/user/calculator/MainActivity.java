package com.example.user.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button btnOne, btnTwo, btnThree, btnFour, btnFive, btnSix, btnSeven, btnEight, btnNine, btnAdd, btnSub, btnDiv, btnMul, btnEquals;
    EditText etOne, etTwo;

    int result = 0;
    int numberOne;
    int numberTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnOne = findViewById(R.id.btnOne);
        btnTwo = findViewById(R.id.btnTwo);
        btnThree = findViewById(R.id.btnThree);
        btnFour = findViewById(R.id.btnFour);
        btnFive = findViewById(R.id.btnFive);
        btnSix = findViewById(R.id.btnSix);
        btnSeven = findViewById(R.id.btnSeven);
        btnEight = findViewById(R.id.btnEight);
        btnNine = findViewById(R.id.btnNine);
        btnAdd = findViewById(R.id.btnAdd);
        btnSub = findViewById(R.id.btnSub);
        btnDiv = findViewById(R.id.btnDiv);
        btnMul = findViewById(R.id.btnMul);
        btnEquals = findViewById(R.id.btnEquals);
        etOne = findViewById(R.id.etOne);
        etTwo = findViewById(R.id.etTwo);

        btnOne.setOnClickListener(onClickListener);
        btnTwo.setOnClickListener(onClickListener);
        btnThree.setOnClickListener(onClickListener);
        btnFour.setOnClickListener(onClickListener);
        btnFive.setOnClickListener(onClickListener);
        btnSix.setOnClickListener(onClickListener);
        btnSeven.setOnClickListener(onClickListener);
        btnEight.setOnClickListener(onClickListener);
        btnNine.setOnClickListener(onClickListener);
        btnAdd.setOnClickListener(onClickListener);
        btnSub.setOnClickListener(onClickListener);
        btnDiv.setOnClickListener(onClickListener);
        btnMul.setOnClickListener(onClickListener);
        btnEquals.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnOne:
                    if (etOne.isFocused()) {
                        String previous = etOne.getText().toString();
                        if (TextUtils.isEmpty(previous)) {
                            etOne.setText("1");
                        } else {
                            etOne.setText(previous + "1");
                        }
                    } else if (etTwo.isFocused()) {
                        String previous = etTwo.getText().toString();
                        if (TextUtils.isEmpty(previous)) {
                            etTwo.setText("1");
                        } else {
                            etTwo.setText(previous + "1");
                        }
                    }
                    break;
                case R.id.btnTwo:
                    if (etOne.isFocused()) {
                        String previous = etOne.getText().toString();
                        if (TextUtils.isEmpty(previous)) {
                            etOne.setText("2");
                        } else {
                            etOne.setText(previous + "2");
                        }
                    } else if (etTwo.isFocused()) {
                        String previous = etTwo.getText().toString();
                        if (TextUtils.isEmpty(previous)) {
                            etTwo.setText("2");
                        } else {
                            etTwo.setText(previous + "2");
                        }
                    }
                    break;
                case R.id.btnThree:

                    break;
                case R.id.btnAdd:
                    numberOne = Integer.parseInt(etOne.getText().toString());
                    numberTwo = Integer.parseInt(etTwo.getText().toString());
                    result = numberOne + numberTwo;
                    break;
                case R.id.btnEquals:
                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                    intent.putExtra("result", result);
                    startActivity(intent);
                    break;

            }
        }
    };

}
